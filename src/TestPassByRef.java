
public class TestPassByRef {
	public void modify(Human h){
		h =  new Human(0,"Jack",10);
		h.setName("Harry");
		h.setAge(40);
		System.out.println(h);
	}
	public void modify2(Human h){
		h.setAge(20);
		h.setName("Tom");
		System.out.println(h);
	}
	public static void main(String[] args) {
		Human p = new Human(0,"Jack",10);
		TestPassByRef test = new TestPassByRef();
		test.modify(p);
		System.out.println(p);
		System.out.println("----------------------------------");
		test.modify2(p);
		System.out.println(p);
		
	}
}
