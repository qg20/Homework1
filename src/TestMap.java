import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;


public class TestMap {
	private Map<Human,String> people = new HashMap<Human,String>();
	
	@Before
	public void testPut(){
		people.put(new Human(0,"Harry",20),"Harry");
		people.put(new Human(1,"Jerry",21),"Jerry");
		people.put(new Human(2,"Tom",22),"Tom");
	}
	@Test
	public void testGet(){
		Human p = new Human(0,"Harry",20);
		String name = people.get(p);
		System.out.println(name);
	}
	@Test
	public void testContainsKey(){
		Human p = new Human(0,"Harry",20);
		boolean flag = people.containsKey(p); 
		System.out.println("Is there a guy named Harry: "+ flag);				
	}
	
	
}
