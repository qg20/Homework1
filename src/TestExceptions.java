import org.junit.Test;
public class TestExceptions {
	//runtime exceptions
	@Test
	public void testNullPointerException(){
		String str = null;
		System.out.println(str.length());
	}
	@Test
	public void testArrayIndexOfBoundsException(){
		int[] arr = {1,2,3};
		System.out.println(arr[4]);		
	}
	
	@Test
	public void testArithmeticException(){
		System.out.println(11/0);
	} 
	
	@Test
	public void testClassCastException(){
		Object obj = "hello";
		Integer i = (Integer)obj;
	}
	@Test
	public void testNumberFormatException(){
		double val = Double.parseDouble("a");		
	}
	
}
