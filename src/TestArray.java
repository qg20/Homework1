import java.util.Arrays;

import org.junit.Test;

public class TestArray {
	@Test
	public void testInitialize(){
		int[] arr = {1,2,3,4};
		int[] arr2;
		arr2 = new int[]{1,2,3,4};
	}
	@Test
	public void testLength(){
		int[] arr = {1,2,3,4};
		int len = arr.length;
		System.out.println("the length is : "+len);
	}
	@Test
	public void testTraversal(){
		int[] arr = new int[5];
		for(int i = 0; i < arr.length; i++){
			arr[i] = i;
		}
	}
	@Test
	public void testCopy(){
		int[] arr1 = {1,2,3,4,5};
		int[] arr2 = new int[5];
		System.arraycopy(arr1, 1, arr2, 0, 3);
		System.out.print("testCopy result: ");
		for(int i = 0; i < arr2.length; i++){
			System.out.print(arr2[i]+" ");
		}
		System.out.println();
	}
	@Test
	public void testCopy2(){
		int[] arr1 = {1,2,3,4,5};
		int[] arr2 = Arrays.copyOf(arr1,6);
		System.out.print("testCopy2 result: ");
		for(int i = 0; i < arr2.length; i++){
			System.out.print(arr2[i]+" ");
		}
		System.out.println();		
	}
	@Test
	public void testExpansion(){
		int[] arr = {1,2,3,4,5};
		arr = Arrays.copyOf(arr, arr.length + 1);	
		System.out.print("testExpansion result: ");
		for(int i = 0; i < arr.length; i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
	@Test
	public void testSort(){
		int[] arr = {1,3,4,2,0,5,10,6};
		Arrays.sort(arr);
		System.out.print("testSort result: ");
		for(int i = 0; i < arr.length; i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();
		
	}
}
