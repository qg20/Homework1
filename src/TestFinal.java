
public class TestFinal {
	private final int num = 1;
	public void testFinal(){
		num = 2;  // compile error: cannot modify final variable!
	}
}
// we cannot modify the final methods and the final class cannot be inherited