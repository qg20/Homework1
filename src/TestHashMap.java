import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;


public class TestHashMap {
	// use map to filter data to get Max value of every character
	Map<String,Integer> map = new HashMap<String,Integer>();
	
	@Before
	public void testInput(){
		// keySet
		String parameter = "A=200,B=201,C=232,D=203,E=204,F=205,G=206,H=207,"
				+ "I=208,J=200,K=203,A=210,B=205,C=209,D=202,E=211,F=222,"
				+ "G=234,H=211,I=221,J=215,K=216,A=211,B=217,C=219,D=220";
		
		String[] arr = parameter.split("[,=]");
		for(int i = 0; i < arr.length; i += 2){
			if(!map.containsKey(arr[i]) || Integer.parseInt(arr[i+1]) > map.get(arr[i])){
				map.put(arr[i],Integer.parseInt(arr[i+1]));
			}
		}
		System.out.println(map);	
	}
	
	@Test
	public void testKeySet(){
		//keySet
		Set<String> keys = map.keySet();
		for(String key : keys){
			System.out.println(key + " : " + map.get(key));
		}
		
	}
	@Test
	public void testEntrySet(){
		//entrySet	
		Set<Map.Entry<String, Integer>> entrys = map.entrySet();
		for(Map.Entry<String, Integer> entry : entrys){
			System.out.println(entry.getKey()+ " : "+ entry.getValue());
		}
	}
	
}
