import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class TestList {

	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		
		// add
		list.add("java");
		list.add("swift");
		list.add("php");
		list.add("c++");
		
		// get
		for(int i = 0; i < list.size(); i++){
			System.out.println(list.get(i).toUpperCase());
		}
		
		//sort
		List<Human> listOfHuman = new ArrayList<Human>();
		listOfHuman.add(new Human(0,"Jack",30));
		listOfHuman.add(new Human(1,"Jack",20));
		listOfHuman.add(new Human(2,"Jack",23));
		System.out.println("Before sorting: " + listOfHuman);
		Collections.sort(listOfHuman, new Comparator<Human>(){
			public int compare(Human p1, Human p2){
				return p1.getAge() - p2.getAge();
			}
		});
		System.out.println("After sorting (based on age): " + listOfHuman);
		
		// set
		String val = list.set(1,"c#");
		System.out.println(list);
		list.set(1,list.set(3,list.get(1)));// exchange elements at 1 and 3
		System.out.println(list);
		// sublist
		List<String> sublist = list.subList(1, 3); // range: [1,3)
		sublist.set(0,sublist.get(0)+".txt");
		sublist.set(1,sublist.get(1)+".txt");
		System.out.println("sublist: "+ sublist);
		System.out.println("list: "+ list);
		sublist.clear();
		System.out.println("cleared sublist: "+ sublist);
		System.out.println("list: "+ list);
		
		// list -> array
		String[] strArr = list.toArray(new String[]{});
		System.out.println("list -> strArr: "+ Arrays.toString(strArr));
		List<String> list2 = Arrays.asList(strArr);
		System.out.println("strArr -> list2: "+ list2);
		
		
	}

}
