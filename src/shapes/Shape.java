package shapes;

public abstract class Shape {
	protected double c;  //perimeter
	public abstract double area();
}
