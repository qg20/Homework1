package shapes;

public class Circle extends Shape {
	public Circle(double c){
		this.c = c;
	}
	@Override
	public double area() {
		return 0.0796*c*c;
	}

}
