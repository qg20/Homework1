package shapes;

public class TestShape {

	public static void main(String[] args) {		
		Shape circle = new Circle(4);
		Shape square = new Square(4);
		System.out.println(circle.area());
		System.out.println(square.area());		
	}

}
