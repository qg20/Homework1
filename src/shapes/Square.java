package shapes;

public class Square extends Shape{
	public Square(double c){
		this.c = c;
	}

	@Override
	public double area() {
		return 0.0625*c*c;
	}
	
}
