
public class TestPassByVal {

	public void modify(int num){
		num++; //  "num" is the copy of "val"
		System.out.println("the modified num is : "+ num); //1
	}
	
	public static void main(String[] args) {
		TestPassByVal test = new TestPassByVal();
		int val = 0;
		test.modify(val);
		System.out.println("the num which is passed into method is : " + val);//0
	}

}
