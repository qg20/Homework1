
public class TestPrimitives {

	public static void main(String[] args) {
		// eight primitive types
		byte x = 1;
		
		short y = 2;
		
		int a = 100000;
		int b = 0x186a0;
		int c = 0303240;
		int d = 5/3;
		System.out.println(d);
		
		long al = 10000000000l;
		long dl = 10000*365*24*60*60*299792458l;
		
		double pi = 3.14;
		double r = 8;
		double s = pi * r * r;
		System.out.println("s = " + s);
		
		float f = 3.14f;
		
		char ch1 = '中';
		char ch2 = '\u4e2d';
		char symbol = '\\';
		System.out.println(ch1);
		System.out.println(ch2);
		System.out.println(symbol);
		
		boolean isChiled = a < 1;
		
		// Wrapper Class
		// Number -> Byte Double Float Integer Long Short 
		
		Number num1 = 1.1;
		Number num2 = 1;
		System.out.println(num1.getClass().getName()); // java.lang.Double
		System.out.println(num2.getClass().getName()); // java.lang.Integer
		
		int intNum = num1.intValue();
		double doubleNum = num1.doubleValue();
		System.out.println(intNum + "," + doubleNum); // 1, 1.1 
		
		intNum = num2.intValue();
		doubleNum = num2.doubleValue();
		System.out.println(intNum + "," + doubleNum); // 1, 1.0 
		
		//Integer
		//String -> int
		String str = "123";
		int val = Integer.parseInt(str); // static method of Integer to transform string to int
		System.out.println(val); // 123
		
		//Double
		String str2 = "123.45";
		double val2 =  Double.parseDouble(str2);//static method of Double
		System.out.println(val2); // 123.45
		
		//autoboxing & unboxing
		Integer n1 = 100; // autoboxing
		Integer n2 = 200; // autoboxing
		Integer n3 = n1 + n2; // unboxing and then autoboxing
		int n4 = n3; // unboxing
		
		Character chObject = 'a';
		System.out.println(chObject.charValue()); 
		
		Boolean flag = true;
		System.out.println(flag.booleanValue());
		
		
	}

}
