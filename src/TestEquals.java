
public class TestEquals {

	public static void main(String[] args) {
		Human person1 = new Human(01,"Jerry",23);
		Human person2 = new Human(02,"Tom",23);
		Human person3 = new Human(01,"Harry",23);
		if(person1.equals(person2)){
			System.out.println("person1 equal person2");
		}
		if(person1.equals(person3)){
			System.out.println("person1 equal person3"); // true, depends on "id"
		}
		if(person2.equals(person3)){
			System.out.println("person2 equal person3");
		}
		if(person1 == person2){
			System.out.println("person1 == person2");
		}
		if(person1 == person3){
			System.out.println("person1 == person3");
		}
		if(person2 == person3){
			System.out.println("person3 == person3");
		}
		
		// String equal
		String s1 = new String("abc");
		String s2 = new String("abc");
		String s3 = "abc";
		String s4 = "abc";
		System.out.println(s1.equals(s2));//true: compare string value
		System.out.println(s1 == s2);//false:  compare reference
		System.out.println(s3 == s4);//true
		System.out.println(s1.equals(s3));//true
		
	}

}
