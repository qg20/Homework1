public class TestStatic {
	private int age;
	private static int numOfStu;	
	public TestStatic(int age){
		this.age = age;
		System.out.println(++numOfStu);
	}
	//execute only one time
	static{  
		System.out.println("Load TestStaticFinal.class");
	}
	//static method belongs to class
	public static void printInfo(){
		System.out.println("Some Info");
	}
	public static void main(String[] args) {		
		TestStatic test = new TestStatic(20);
		TestStatic test2 =  new TestStatic(30);
		TestStatic test3 =  new TestStatic(40);
		TestStatic.printInfo();
		System.out.println(TestStatic.numOfStu);
	}	
}

