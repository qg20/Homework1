import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class TestCollections {
	public static void main(String[] args) {
		// collection contains references of elements
		Collection<Human> human_collection = new ArrayList<Human>();
		human_collection.add(new Human(1,"Jack",20));
		Human person = new Human(2,"Harry",21);
		human_collection.add(person);
		System.out.println(person);
		System.out.println(human_collection);
		
		person.setName("Harry2");
		System.out.println(person);
		System.out.println(human_collection);
		
		// contains: according to equals()
		Human person2 = new Human(3,"Harry",21);
		Human person3 = new Human(1,"Harry",21);// id of person3 = id of person1
		System.out.println(human_collection.contains(person2)); //false
		System.out.println(human_collection.contains(person3));// true
		
		//size
		System.out.println("collection's size is : " + human_collection.size()); // 2
		
		//isEmpty
		System.out.println("collection now is empty: " + human_collection.isEmpty());// false
		
		//addAll
		Collection<Human> human_collection2 = new ArrayList<Human>();
		human_collection2.add(new Human(4,"Lucy",20));
		human_collection2.add(new Human(5,"Lily",20));
		System.out.println(human_collection2);
		human_collection.addAll(human_collection2);
		System.out.println("human_collection now is: " + human_collection);
	
		//containsAll
		System.out.println("human_collection now contains all human_collection2: " + human_collection.containsAll(human_collection2));
		
		//iterator remove
		Iterator<Human> it = human_collection.iterator();
		while(it.hasNext()){
			Human p = it.next();
			System.out.println(p);
			if(p.getName().equals("Jack")){
				it.remove();
			}
		}
		System.out.println(human_collection);
		
		//enhanced for loop
		for(Human p:human_collection){
			System.out.print(p.getName().toUpperCase()+ " ");
		}
		System.out.println();
		
		//clear
		human_collection.clear();
		System.out.println("collection's size is : " + human_collection.size()); //0
		System.out.println("collection now is empty: " + human_collection.isEmpty());// true
	}
}
